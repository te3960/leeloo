import { TezosToolkit } from '@taquito/taquito';
import { BeaconWallet } from '@taquito/beacon-wallet';
import { char2Bytes } from '@taquito/utils';

const Tezos = new TezosToolkit('https://mainnet-tezos.giganode.io');
const wallet = new BeaconWallet({ name: 'Leeloo' });
Tezos.setWalletProvider(wallet);

const methods = {
	init: async function () {
		const activeAccount = await wallet.client.getActiveAccount();
		if (activeAccount) {
			state.user = activeAccount;
		}
	},
	connectWallet: async function () {
		try {
			await wallet.client.requestPermissions();
			await this.init();
			return true;
		} catch (error) {
			console.log('Got wallet connection error:', error);
			return false;
		}
	},
	logout: async function () {
		await wallet.clearActiveAccount();
		state.user = null;
	},
	sign: async function () {
		try {
			let msg = 'sign:' + state.user.address;
			const bytes = char2Bytes(msg);
			const payloadBytes = '05' + '01' + char2Bytes(bytes.length.toString()) + bytes;
			const response = await wallet.client.requestSignPayload({
				signingType: 'micheline',
				payload: payloadBytes,
			});
			return {signature: response.signature, payload: payloadBytes};
		} catch (error) {
			console.log('Got sign error:', error);
			return null;
		}
	},
};

const state = {
	wallet: wallet,
	user: null,
	...methods,
};

export default state;
