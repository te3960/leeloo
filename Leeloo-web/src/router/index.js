import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import AccessRules from '../pages/AccessRules.vue'
import CreateAccessRules from '../pages/CreateAccessRules.vue'
import BalancePage from '../pages/BalancePage.vue'
import ProfilePage from '../pages/ProfilePage.vue'
import CasteCreate from '../pages/CasteCreate.vue'
import RoomsPage from '../pages/RoomsPage.vue'
import CastesPage from '../pages/CastesPage.vue'
import JoinPage from '../pages/JoinPage.vue'
import SubscribesPage from '../pages/SubscribesPage.vue'


Vue.use(VueRouter)

const routes = [
  { path: '/AccessRules', component: AccessRules },
  { path: '/CreateAccessRules', component: CreateAccessRules },
  { path: '/Balance', component: BalancePage },
  { path: '/Profile', component: ProfilePage },
  { path: '/CasteCreate', component: CasteCreate },
  { path: '/Rooms', component: RoomsPage },
  { path: '/Castes', component: CastesPage },
  { path: '/Join/:casteId', component: JoinPage, props: true },
  { path: '/Subscribes', component: SubscribesPage, props: true },
  
  {
    path: '/',
    name: 'Home',
    component: Home
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
