import { createRequire } from 'module';
const require = createRequire(import.meta.url);

const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectId;
import { Logger } from 'mongodb';

const mongoClient = new MongoClient('mongodb://localhost:27017/', { monitorCommands: true });

mongoClient.on('commandStarted', event => console.debug(event.command));
mongoClient.on('commandFailed', event => console.debug(event));

await mongoClient.connect();
let db = mongoClient.db('Leeloo');
console.log('Database connected.');

export default {
	getAllAccessRules: async function () {
		let c = db.collection('accessRules');
		return await c.find({}).toArray();
	},
	getAccessRules: async function (id) {
		let c = db.collection('accessRules');
		return await c.findOne({ _id: ObjectId(id) });
	},
	addAccessRules: async function (accessRule) {
		let c = db.collection('accessRules');
		let obj = { tokenLimits: accessRule };
		let { insertedId } = await c.insertOne(obj);
		return insertedId;
	},
	getUser: async function (userId) {
		let c = db.collection('users');
		return await c.findOne({ _id: userId });
	},
	addUser: async function (userId, pk = '') {
		let c = db.collection('users');
		await c.insertOne({ _id: userId, pk: pk });
	},
	saveUser: async function (userId, telegramId, discordId) {
		let c = db.collection('users');
		await c.updateOne({ _id: userId }, { $set: { telegramId: telegramId, discordId: discordId } });
	},
	saveUserDiscordApp: async function (userId, discordUserId, access_token, expires_in, scope) {
		let c = db.collection('users');
		await c.updateOne(
			{ _id: userId },
			{
				$set: {
					discordUserId: discordUserId,
					discordAppConnected: true,
					access_token: access_token,
					expires_in: expires_in,
					scope: scope,
					discordAppAt: new Date().toISOString(),
				},
			}
		);
	},
	getUserBalance: async function (userId) {
		let c = db.collection('balances');
		return await c.findOne({ _id: userId });
	},
	saveUserBalance: async function (userId, balances) {
		let c = db.collection('balances');
		//await c.replaceOne( { _id: userId}, {  balances}, { upsert: true });
		await c.updateOne({ _id: userId }, { $set: { balances: balances } }, { upsert: true });
	},
	addRoom: async function (room) {
		let c = db.collection('rooms');
		let { insertedId } = await c.insertOne(room);
		return insertedId;
	},
	getAllUserRooms: async function (userId) {
		let c = db.collection('rooms');
		return await c.find({ userId: userId }).toArray();
	},
	getUserRooms: async function (userId, ids) {
		let c = db.collection('rooms');
		var objectIds = ids.map(i => ObjectId(i));
		return await c.find({ userId: userId, _id: { $in: objectIds } }).toArray();
	},
	getRooms: async function (ids) {
		let c = db.collection('rooms');
		var objectIds = ids.map(i => ObjectId(i));
		return await c.find({ _id: { $in: objectIds } }).toArray();
	},
	getRoomByServer: async function (serverId, botInGroup = true) {
		let c = db.collection('rooms');
		return await c.findOne({ botInGroup: botInGroup, serverId: serverId });
	},
	addCaste: async function (userId, caste) {
		let c = db.collection('castes');
		let { insertedId } = await c.insertOne({ userId: userId, ...caste });
		return insertedId;
	},
	getUserCastes: async function (userId) {
		let c = db.collection('castes');
		return await c.find({ userId: userId }).toArray();
	},
	getCaste: async function (casteId) {
		let c = db.collection('castes');
		return await c.findOne({ _id: ObjectId(casteId) });
	},
	getCastes: async function (ids) {
		let c = db.collection('castes');
		var objectIds = ids.map(i => ObjectId(i));
		return await c.find({ _id: { $in: objectIds } }).toArray();
	},
	casteInteraction: async function (casteId) {
		let c = db.collection('castes');
		await c.updateOne({ _id: ObjectId(casteId) }, { $inc: { InteractionCount: 1 } });
	},

	addSubscribe: async function (userId, casteId) {
		let c = db.collection('subscribes');
		let rez = await c.updateOne({ _id: userId }, { $addToSet: { subs: casteId } }, { upsert: true });
		return rez.upsertedCount + rez.modifiedCount > 0;
	},
	getSubsribes: async function (userId) {
		let c = db.collection('subscribes');
		return await c.findOne({ _id: userId });
	},
	isSubscriber: async function (userId, casteId) {
		let c = db.collection('subscribes');
		return (await c.countDocuments({ _id: userId, subs: casteId })) != 0;
	},

	addCasteFollower: async function (userId, casteId) {
		let c = db.collection('casteFollowers');
		let rez = await c.updateOne({ _id: casteId }, { $addToSet: { followers: userId } }, { upsert: true });
		return rez.upsertedCount + rez.modifiedCount > 0;
	},
	getCasteFollowers: async function (casteId) {
		let c = db.collection('casteFollowers');
		return await c.findOne({ _id: casteId });
	},

	casteSubscriberInc: async function (casteId) {
		let c = db.collection('castes');
		await c.updateOne({ _id: ObjectId(casteId) }, { $inc: { SubcriberCount: 1 } });
	},
	setRoomBotJoin: async function (serverId, isJoin) {
		let c = db.collection('rooms');
		await c.updateMany({ serverId: serverId }, { $set: { botInGroup: isJoin } });
	},
	getCastesWithRoom: async function (roomId) {
		let c = db.collection('castes');
		return await c.find({ 'flows.roomIds': roomId }).toArray();
	},
	saveUserRoomPass: async function (userId, roomId, isJoined = false, info = null, errorMsg = null) {
		let c = db.collection('userRoomPasses');
		let u = { isJoined, info, errorMsg, updateAt: new Date().toISOString() };
		await c.updateOne({ userId: userId, roomId: roomId }, { $set: u }, { upsert: true});
	},
	getUserRoomPasses: async function(userId){
		let c = db.collection('userRoomPasses');
		return await c.find({ userId: userId}).toArray();
	}


/* 	getUserRoom: async function(userId, roomId){
		let c = db.collection('userRooms');
		let res = await c.findOne({_id: userId, 'rooms.roomId': roomId}, { projection: {_id:0, 'rooms.$': 1}});
		return res?.rooms?.[0];
	},
	addUserRoom: async function(userId, roomId, isJoined = false, errorMsg = null){
		let c = db.collection('userRooms');
		let room = { roomId, isJoined, errorMsg: errorMsg, updateAt: new Date().toISOString() };
		await c.updateOne({ _id: userId, 'rooms.roomId': { $ne: roomId }} , { $push: { rooms: room } }, { upsert: true});
	},
	saveUserRoom: async function (userId, roomId, isJoined = false, errorMsg = null) {
		let c = db.collection('userRooms');
		let room = { roomId, isJoined, errorMsg: errorMsg, updateAt: new Date().toISOString() };
		await c.updateOne({ _id: userId }, { $set: { 'rooms.$[e]': room } }, { arrayFilters: [ { "e.roomId": roomId } ] });
	}, */

	// arrayFilters: [ { 'e.roomId': roomId } ],
	//await db.saveUserRoom(job.data.userId, r._id.toString(), false, error.message)
};
