import { createRequire } from 'module';
const require = createRequire(import.meta.url);

var crypto = require('crypto');

const iv = Buffer.from([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
const key = crypto.scryptSync('dskf4353Er', 'salt', 32);

function encrypt(text) {
	if (typeof text != String) {
		text = JSON.stringify(text);
	}
	const cip = crypto.createCipheriv('aes-256-cbc', key, iv);
	let str = cip.update(text, 'utf8', 'hex');
	str += cip.final('hex');
	return str;
}

function decrypt(text) {
	const cip = crypto.createDecipheriv('aes-256-cbc', key, iv);
	let str = cip.update(text, 'hex', 'utf8');
	str += cip.final('utf8');
	return str;
}

export { encrypt, decrypt };
