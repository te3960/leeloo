import { createRequire } from 'module';
const require = createRequire(import.meta.url);
require('dotenv').config();

process.on('uncaughtException', function (err) {
	console.error('core unhandled exception', err);
});

const Bull = require('bull');
const redis = {
	host: process.env.REDIS_HOST,
	port: 6379,
};
const queue = new Bull('main-queue', { redis: redis });

const express = require('express');
const app = express();
const cors = require('cors');
const cookieParser = require('cookie-parser');

app.use(cookieParser());

const host = '127.0.0.1';
const port = 7000;

var corsOptions = {
	credentials: true,
	origin: function (origin, callback) {
		callback(null, true);
	},
};
app.use(cors(corsOptions));

const jsonParser = express.json();

import { encrypt, decrypt } from './encrypt.js';
import { verifySignature, getPkhfromPk } from '@taquito/utils';
import { db as dbProvider } from 'leeloo-share-sv';
import axios from 'axios';

app.use(function (req, res, next) {
	req.auth = null;
	req.isLogin = false;
	let authStr = req.cookies['leeloo'];
	if (authStr) {
		let dec = decrypt(authStr);
		req.auth = JSON.parse(dec);
		req.isLogin = true;
	}
	next();
});

app.get('/getAllAccessRules', async (req, res) => {
	res.status(200).type('text/json');
	res.send(await dbProvider.getAllAccessRules());
});

app.get('/getAccessRules', async (req, res) => {
	res.status(200).type('text/json');
	res.send(await dbProvider.getAccessRules(req.query.id));
});

app.post('/addAccessRules', jsonParser, async (req, res) => {
	res.send(await dbProvider.addAccessRules(req.body));
});

app.post('/login', jsonParser, async (req, res) => {
	//{ signature: sign.signature, payload: sign.payload, pk: auth.user.publicKey }
	if (!verifySignature(req.body.payload, req.body.pk, req.body.signature)) {
		res.status(403).type('text/json');
		res.send('sign verify failed');
		return;
	}
	let address = getPkhfromPk(req.body.pk);
	let user = await dbProvider.getUser(address);
	if (user == null) {
		await dbProvider.addUser(address, req.body.pk);
	}
	res.status(200).type('text/json');
	res.send(encrypt({ address: address, loginAt: new Date().toISOString() }));
});

app.get('/getUser', async (req, res) => {
	res.status(200).type('text/json');
	res.send(await dbProvider.getUser(req.auth.address));
});

app.post('/saveUser', jsonParser, async (req, res) => {
	await dbProvider.saveUser(req.auth.address, req.body.telegramId, req.body.discordId);
	res.status(200).type('text/json');
	res.send('saved');
});

app.post('/saveUserDiscordApp', jsonParser, async (req, res) => {
	let headers = {
		authorization: `Bearer ${req.body.access_token}`,
	};
	let url = 'https://discord.com/api/users/@me';
	let { data } = await axios.get(url, { headers: headers });

	await dbProvider.saveUserDiscordApp(req.auth.address, data.id, req.body.access_token, req.body.expires_in, req.body.scope);
	res.status(200).type('text/json');
	res.send('saved');
});

app.get('/getUserBalance', async (req, res) => {
	res.status(200).type('text/json');
	let balance = await dbProvider.getUserBalance(req.auth.address);
	if (balance == null) {
	}
	res.send(balance);
});

app.post('/addRoom', jsonParser, async (req, res) => {
	let room = {
		userId: req.auth.address,
		adminId: req.body.adminId,
		serverId: req.body.serverId,
		channelId: req.body.channelId,
		type: req.body.type,
		name: req.body.name,
		checkBotInGroup: 'pending',
		checkInGroup: 'pending',
		checkIsAdmin: 'pending',
	};
	let roomId = await dbProvider.addRoom(room);
	res.status(200).type('text/json');
	res.send(roomId);
});

app.get('/getAllRooms', async (req, res) => {
	res.status(200).type('text/json');
	let rooms = await dbProvider.getAllUserRooms(req.auth.address);
	res.send(rooms);
});

app.post('/getRooms', jsonParser, async (req, res) => {
	let rooms = await dbProvider.getUserRooms(req.auth.address, req.body);
	res.status(200).type('text/json');
	res.send(rooms);
});

app.post('/addCaste', jsonParser, async (req, res) => {
	await dbProvider.addCaste(req.auth.address, req.body);
	res.status(200).type('text/json');
	res.send('saved');
});

app.get('/getCastes', async (req, res) => {
	res.status(200).type('text/json');
	let castes = await dbProvider.getUserCastes(req.auth.address);
	res.send(castes);
});

app.get('/getCaste', async (req, res) => {
	res.status(200).type('text/json');
	res.send(await dbProvider.getCaste(req.query.id));
});

app.post('/getCastes', jsonParser, async (req, res) => {
	res.status(200).type('text/json');
	let castes = await dbProvider.getCastes(req.body);
	res.send(castes);
});

app.get('/joinInteraction', async (req, res) => {
	res.status(200).type('text/json');
	res.send(await dbProvider.casteInteraction(req.query.id));
});
app.get('/getUserRoomPasses', async (req, res) => {
	res.status(200).type('text/json');
	res.send(await dbProvider.getUserRoomPasses(req.auth.address));
});

app.post('/subscribe', jsonParser, async (req, res) => {
	if (await dbProvider.addSubscribe(req.auth.address, req.body.casteId)) {
		await dbProvider.addCasteFollower(req.auth.address, req.body.casteId);
		await dbProvider.casteSubscriberInc(req.body.casteId);
		queue.add('userSubToCaste', { userId: req.auth.address, casteId: req.body.casteId });
	}
	res.status(200).type('text/json');
	res.send('saved');
});

app.get('/isSubscriber', async (req, res) => {
	res.status(200).type('text/json');
	res.send(await dbProvider.isSubscriber(req.auth.address, req.query.casteId));
});

app.get('/getSubsribes', async (req, res) => {
	res.status(200).type('text/json');
	let rez = await dbProvider.getSubsribes(req.auth.address);
	res.send(rez?.subs);
});

app.use((req, res, next) => {
	res.status(404).type('text/plain');
	res.send('Not found');
});

app.use(function (err, req, res, next) {
	console.log('Unhandled exception!');
	console.log('Path: ', req.path);
	console.error('Error: ', err);
	res.status(500).send('Something broke!');
});

app.listen(port, host, function () {
	console.log(`Server listens http://${host}:${port}`);
});
