export default {
    tokenBalance: function(data){
        let balances = []
        data.forEach(i => {
            balances.push({
                balance: i.balance,
                firstLevel: i.firstLevel,
                firstTime: i.firstTime,
                lastLevel: i.lastLevel,
                lastTime: i.lastTime,
                tokenId: i.token.tokenId,
                standard: i.token.standard,
                contract: i.token.contract.address,
                name: i.token.contract.alias,
            })
        });
        return balances;
    }
}