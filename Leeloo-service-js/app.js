import { createRequire } from 'module';
const require = createRequire(import.meta.url);
require('dotenv').config();

const Bull = require('bull');
import axios from 'axios';

const redis = {
	host: process.env.REDIS_HOST,
	port: 6379,
};

const queue = new Bull('main-queue', { redis: redis });

import { db } from 'leeloo-share-sv';
import { tzktParser } from 'leeloo-share';

async function updateBalance(userId) {
	let { data } = await axios.get(`https://api.tzkt.io/v1/tokens/balances?account=${userId}&limit=1000`);
	if (!data) {
		return Promise.resolve();
	}
	let userBalance = await db.getUserBalance(userId);
	let maxBlock = userBalance == null ? 0 : Math.max(...userBalance.balances.map(b => b.lastLevel));
	let maxBlockNow = data.length == 0 ? Number.MAX_SAFE_INTEGER : Math.max(...data.map(b => b.lastLevel));
	if (maxBlockNow > maxBlock) {
		db.saveUserBalance(userId, tzktParser.tokenBalance(data));
	}
}

queue.process('updateBalance', async job => {
	let userId = job.data.userId;
	await updateBalance(userId);

	let userFollowings = await db.getSubsribes(userId);
	if (!userFollowings || !userFollowings.subs.length) {
		return Promise.resolve();
	}
	userFollowings.subs.forEach(i => queue.add('casteUserPass', { casteId: i, userId: userId }));
});

queue.process('userSubToCaste', async job => {
	let { userId, casteId } = job.data;
	await updateBalance(userId);
	queue.add('casteUserPass', { casteId: casteId, userId: userId });
});

queue.process('casteUserPass', async job => {
	// verify
	let { casteId, userId } = job.data;
	let userBalance = await db.getUserBalance(userId);
	if (!userBalance) {
		return Promise.resolve();
	}
	let caste = await db.getCaste(casteId);
	if (!caste || !caste.flows.length) {
		return Promise.resolve();
	}

	// prepare | fa2 without tokenId now
	userBalance = userBalance.balances.reduce((res, item) => {
		let obj = res.find(i => i.contract == item.contract);
		if (!obj) {
			res.push({ contract: item.contract, balance: Number(item.balance) });
			return res;
		}
		obj.balance += Number(item.balance);
		return res;
	}, []);
	await Promise.all(
		caste.flows.map(async f => {
			f.tokensRule = (await db.getAccessRules(f.tokenRulesId)).tokenLimits;
		})
	);

	// act
	for (let f of caste.flows) {
		let flowPass = true;
		for (let tr of f.tokensRule) {
			let ub = userBalance.find(i => i.contract == tr.address);
			if (!ub || ub.balance < tr.min) {
				flowPass = false;
				break;
			}
		}
		flowPass ? queue.add('joinUserToRooms', { userId: userId, roomIds: f.roomIds }) : queue.add('kickUserFromRooms', { userId: userId, roomIds: f.roomIds });
	}
});

queue.process('kickUserFromRooms', async job => {
	console.log('kick user');
	let rooms = await db.getRooms(job.data.roomIds);
	let user = await db.getUser(job.data.userId);

	for (let r of rooms) {
		if (r.type == 'discord') {
			const guild = client.guilds.cache.get(r.serverId);
			if (guild == null) {
				console.log('bot isnt join to server: ' + r.serverId);
				continue;
			}
			try {
				await guild.members.kick(user.discordUserId);
				await db.saveUserRoomPass(job.data.userId, r._id.toString(), false, 'kicked');
			} catch (error) {
				await db.saveUserRoomPass(job.data.userId, r._id.toString(), false, 'error cant kick', error.message);
				console.log('kick to room discord error: ', error.message);
			}
		}
	}
});

queue.process('joinUserToRooms', async job => {
	console.log('join user');
	let rooms = await db.getRooms(job.data.roomIds);
	let user = await db.getUser(job.data.userId);

	for (let r of rooms) {
		if (r.type == 'discord') {
			const guild = client.guilds.cache.get(r.serverId);
			if (guild == null) {
				console.log('bot isnt join to server: ' + r.serverId);
				continue;
			}
			try {
				await guild.members.add(user.discordUserId, { accessToken: user.access_token });
				await db.saveUserRoomPass(job.data.userId, r._id.toString(), true, 'joined');
			} catch (error) {
				await db.saveUserRoomPass(job.data.userId, r._id.toString(), false, 'error cant join', error.message);
				console.log('join to room discord error: ', error.message);
			}
		}
	}
});

queue.process('caste:discord:botJoinToServer', async job => {
	let { serverId } = job.data;
	let room = await db.getRoomByServer(serverId, true);
	if (!room) {
		return Promise.resolve();
	}
	let castes = await db.getCastesWithRoom(room._id.toString());
	castes.forEach(async i => {
		let followers = (await db.getCasteFollowers(i._id.toString()))?.followers ?? [];
		followers.forEach(f => {
			queue.add('casteUserPass', { casteId: i._id.toString(), userId: f });
		});
	});
});

// ------------ discord ------------
const { Client, Intents } = require('discord.js');
let botToken = process.env.DISCORD_BOT_TOKENID;

const myIntents = new Intents();
myIntents.add(Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MEMBERS, Intents.FLAGS.GUILD_INVITES);

const client = new Client({ intents: myIntents });

client.on('ready', () => {
	console.log(`Logged in as ${client.user.tag}!`);
});

// bot added
client.on('guildCreate', async guild => {
	console.log('bot join to new guild: ' + guild.id);
	await db.setRoomBotJoin(guild.id, true);
	queue.add('caste:discord:botJoinToServer', { serverId: guild.id });
});

// bot kick
client.on('guildDelete', async guild => {
	console.log('bot kick from guild: ' + guild.id);
	await db.setRoomBotJoin(guild.id, false);
});

await client.login(botToken);

//queue.add('updateBalance', { userId: 'tz1g1Hytu5LBwjoye66d6hkJkgwxBdXD8ZG8' });
//queue.add('caste:discord:botJoinToServer', { serverId: '945070343826067526' });

console.log('serverjs_finish');
